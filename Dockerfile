FROM python:3.8-alpine

WORKDIR /project
ADD . /project

RUN apk update
RUN apk add --no-cache musl-dev libffi-dev openssl-dev python-dev postgresql-libs postgresql-dev gcc make

RUN pip install poetry
# installs dependency in the host os rather than inside a virtualenv
ENV POETRY_VIRTUALENVS_CREATE=false
RUN poetry install
