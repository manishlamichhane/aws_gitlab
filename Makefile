all: run migrate build test

run:
	./manage.py runserver

migrate:
	./manage.py migrate

build:
	# collects static files
	./manage.py collectstatic --noinput

test:
	# -x: stops test run when first test fails
	# -v: show verbose information about which test is being executed
	# --no-migration: creates db as the current state of models, without migrations (hence, faster)
	# -n auto: uses all the cores in CPU possible (tests execute faster)
	pytest -v -x --no-migrations -n auto

deploy:
	eb